﻿using CmsShpoingcart.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CmsShpoingcart.Models.ViewModels.Pages
{
    public class PageVm

    {
        public PageVm() {
        }
        public PageVm(PageDto row){
            Id = row.Id;
            Title = row.Title;
            Body = row.Body;
            Sorting = row.Sorting;
            Slug = row.Slug;
            HasSidebar = row.HasSidebar;



        }
        public int Id { get; set; }
        [Required]
        [StringLength(50,MinimumLength =3)]
        public string Title { get; set; }
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 3)]

        public string Body { get; set; }
        public string Slug{ get; set; }
        public int Sorting { get; set; }
        public bool HasSidebar { get; set; }
    }
}