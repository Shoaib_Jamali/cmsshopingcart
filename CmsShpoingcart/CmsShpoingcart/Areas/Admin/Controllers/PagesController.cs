﻿using CmsShpoingcart.Models.Data;
using CmsShpoingcart.Models.ViewModels.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CmsShpoingcart.Areas.Admin.Controllers
{
    public class PagesController : Controller
    {
        // GET: Admin/Pages
        public ActionResult Index()
        {
            //Declear ListOf pagesVm
            List<PageVm> pagesList;
            using (Db db=new Db()) {
                pagesList = db.Pages.ToArray().OrderBy(x => x.Sorting).Select(x => new PageVm(x)).ToList();

                }
                return View(pagesList);
        }
        [HttpGet]
        // GET: Admin/Pages/AddPage
        public ActionResult AddPage()
        {
            return View();
        }
        /*
        // POST: Admin/Pages/AddPage
        [HttpPost]
        public ActionResult AddPage(PageVm model)
        {
            //check the Model state
            if (!ModelState.IsValid) {
                return View(model);
            }
            using (Db db = new Db()) {
               
            }

            //Declear the Slug
            string slug;
            //Init PageDto
            PageDto dto = new PageDto();

            //DTO Title
            dto.Title = model.Title;
            //check for and set slug if need be
            if (string.IsNullOrWhiteSpace(model.Slug))
            {

                slug = model.Slug.Replace("", "-").ToLower();
            }
            else {
                slug = model.Slug.Replace("", "-").ToLower();
            }
                //make sure title and slug are unique


                //Dto the Rest

                // And save the dto

                //set temp data message

                // redirect
                return View();
        }
        */
    }
}